<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('admin/home');
		$this->load->view('footer');
	}

	public function add_cashier()
	{
		if ($this->session->userdata('is_admin') == true) {
			$this->load->view('header');
			$this->load->view('admin/add_cashier');
			$this->load->view('footer');
		}else{
			$this->session->set_flashdata('alert', 'You were Logged Out');
			redirect('index.php/login','refresh');
		}
	}

	public function add_cashier_process()
	{
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$nama				= $this->input->post('name');

		if ($this->session->userdata('is_admin') == TRUE) {
			if ($this->user_model->create_cashier($username,$password,$nama)) {
				$this->session->set_flashdata('alert', 'Berhasil menambahkan akun kasir');
			}else{
				$this->session->set_flashdata('alert', 'gagal menambahkan akun kasir');
			}

			redirect('index.php/admin/add_cashier','refresh');
		}else{
			redirect('index,php/login','refresh');
		}
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */