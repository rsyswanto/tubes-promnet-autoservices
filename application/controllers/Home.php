<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('parts_model');
		$this->load->model('services_model');
	}

	public function index()
	{
		if ($this->session->userdata('is_cashier') == true) {
			$this->load->view('header');
			$this->load->view('home/dashboard');
			$this->load->view('footer');
		}else{
			$this->session->set_flashdata('alert', 'you were logged out');

			redirect(base_url('index.php/login/logout'),'refresh');
		}
	}

	public function transaction()
	{
		if ($this->session->userdata('is_cashier') == true) {
			/**/
			
			/**/

			$this->load->view('header');
			$this->load->view('home/home_view');
			$this->load->view('footer');
		}else{
			$this->session->set_flashdata('alert', 'you were logged out');
			redirect(base_url('index.php/login/logout'),'refresh');
		}
	}

	public function tambahTransaksi()
	{
		if ($this->session->userdata('is_cashier') == true) {
			$plg = array(
				'plgNama' => $this->input->post('fnama'),
				'plgKendaraan' => $this->input->post('fkendaraan'),
				'plgNopol' => $this->input->post('fnopol'),
				'plgTelepon' => $this->input->post('ftelepon'),
				'plgNoTransaksi' => "0021",
				'plgWaktu' => mdate("%Y-%m-%d %h:%i %A")
			);
			
			$this->session->set_userdata( $plg );

			// $this->transaction();
			redirect(base_url('index.php/home/transaction'));
		}else{
			$this->session->set_flashdata('alert', 'you were logged out');
			redirect(base_url('index.php/login/logout'),'refresh');
		}
	}

	public function listParts()
	{
		$data['parts'] = $this->parts_model->getPartsData();
	}

	public function listServices()
	{
		$data['services'] = $this->services_model->getAll();
	}

	public function dashboard()
	{
		$this->load->view('header');
		$this->load->view('home/dashboard');
		$this->load->view('footer');
	}

	public function purchase()
	{
		$this->load->view('header');
		$this->load->view('home/purchase_view');
		$this->load->view('footer');
	}

	public function search()
	{
		# code...
	}

	public function FunctionName($value='')
	{
		# code...
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */