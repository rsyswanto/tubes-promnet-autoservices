<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// code
		$this->load->model('user_model');
	}

	public function index()
	{

		if (!$this->session->has_userdata('user_id')) {
			// validasi gagal
			echo "does not have user data";

			$this->load->view('header');
			$this->load->view('login/login_view');
			$this->load->view('footer');

		} else {
			if ($this->session->userdata('is_admin')) {
				redirect('index.php/admin/add_cashier','refresh');
			}else if ($this->session->userdata('is_cashier')) {
				redirect('index.php/home/','refresh');
			}else{
				$this->session->set_flashdata('alert', 'You Were Logged Out');
				redirect(base_url('index.php/login/logout'),'refresh');
			}
			
		}
	}

	public function login_Process()
	{
		// set variabel dari form
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if ($this->user_model->pass_check($username,$password)) {
			// ambil id user berdasarkan username
			// echo "pass_check";
			$user_id = $this->user_model->get_user_id_from_username($username);
			// ambil object berdasarkan id user
			$user = $this->user_model->get_user_detail($user_id);

			// set array untuk session
			$array = array(
				'user_id'	 		=> (int)$user->id,
				'username' 		=> (string)$user->username,
				'name'				=> (string)$user->nama,
				'is_cashier'	=> (bool)$user->is_cashier,
				'is_admin'		=> (bool)$user->is_admin,
			);
			// set session
			$this->session->set_userdata( $array );

			// load view jika user login OK
			if ($this->session->userdata('is_cashier') == TRUE) {
				redirect('index.php/home/','refresh');
			}else if ($this->session->userdata('is_admin') == TRUE) {
				redirect('index.php/admin/add_cashier');
			}
		}else{
			$data['error'] = 'Wrong username or password';

			
			$this->load->view('header');
			$this->load->view('login/login_view', $data);
			$this->load->view('footer');
		}
	}

	/**
	 * logout function
	 * loging out and back to login page
	 * 
	 * @access public
	 * @return void
	 */
	public function logout()
	{
		if ($this->session->has_userdata('user_id')) {
			$sessdata = array(
				'user_id',
				'username',
				'name',
				'is_cashier',
				'is_admin'
			);

			$this->session->unset_userdata($sessdata);
		}

		$this->session->sess_destroy();

		redirect('index.php/login');
	}

	/**
	 * create_admin function
	 * membuat admin dengan kode
	 * 
	 * @access public
	 * @return void
	 */
	public function create_admin()
	{

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$name 		= $this->input->post('name');
		$code 		= $this->input->post('code');

		$passcode = "secretcode";
		if ($code === $passcode) {
			if ($this->user_model->create_admin($username,$password,$name)) {
				$this->session->set_flashdata('notif', 'Administrator baru berhasil ditambahkan');
			}else{
				$this->session->set_flashdata('notif', 'Gagal menambahkan Administrator');
			}
		}else{
			$this->session->set_flashdata('notif', 'Kode Salah');
			redirect('index.php/login/view_create_admin');
		}
		
		redirect('index.php/login');
	}

	public function view_create_admin()
	{
		$this->load->view('header');
		$this->load->view('login/tambah_admin_view');
		$this->load->view('footer');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */