<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
			
			<div class="list-group">
			    <a href="#" class="list-group-item active waves-effect">Halaman Utama</a>
			    <a href="<?php echo base_url('index.php/admin/add_cashier') ?>" class="list-group-item list-group-item-action waves-effect">Tambah Akun Kasir</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Rekapitulasi</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Stok Barang</a>
			</div>
                
		</div> <!-- /.col-md -->

		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
			<h1>Welcome, Admin!</h1>
		</div>
	</div> <!-- /row -->
</div> <!-- /container -->
