<!DOCTYPE html>
<html lang="en">

<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title></title>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Bootstrap core CSS -->
		<link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
		<!-- Material Design Bootstrap -->
		<link href="<?php echo base_url('assets/css/mdb.min.css') ?>" rel="stylesheet">
		<!-- Your custom styles (optional) -->
		<link href="<?php echo base_url('assets/css/style.css"') ?> rel="stylesheet">
</head>

<body style="background-color: #F5F5F5">

<!-- header -->
<header id="header" class="">

	<nav class="navbar navbar-dark unique-color justify-content-between">
    <span>
    	<a class="navbar-brand" href="#">AutoServices</a>
    </span>
    <span class="navbar-text text-white">
    	<?php if ($this->session->has_userdata('user_id')): ?>
	    	 Masuk sebagai <?php echo $this->session->userdata('name'); ?>
	    <?php else: ?>
	    	Selamat Datang
    	<?php endif ?>
		</span>
		<span>
			<?php if ($this->session->has_userdata('user_id')): ?>
				<a class="btn btn-sm btn-danger" href="<?php echo base_url('index.php/login/logout') ?>">lOGOUT</a>
			<?php else: ?>
				<a class="btn btn-sm btn-primary" href="<?php echo base_url('index.php/login/view_create_admin') ?>">Tambah Admin</a>
			<?php endif ?>


		</span>
</nav>

</header>


<!-- /.header -->
<br>

		<!-- <?php if (isset($_SESSION)) : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php var_dump($_SESSION); ?>
					</div>
				</div>
			</div>
		<?php endif; ?> -->
