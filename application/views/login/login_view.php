<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
	<?php if (validation_errors()) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="col-md-3"></div>
		<div class="col-md">
			<div class="page-header">
				<h1>Login</h1>
			</div>
			<hr>
			<?= form_open(base_url('index.php/login/login_Process')) ?>
				<div class="form-group">
					<label for="username" class="grey-text">Username</label>
					<input type="text" class="form-control" id="username" name="username" placeholder="">
				</div>
				<div class="form-group">
					<label for="password" class="grey-text">Password</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-outline-default" value="Login">
				</div>
			<?php echo form_close() ?>
			<hr>
		</div>
	<div class="col-md-3"></div>
</div><!-- .row -->
