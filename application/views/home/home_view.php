<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
			<div class="list-group">
				<a href="<?php echo base_url('index.php/home/') ?>" class="list-group-item list-group-item-action waves-effect">Transaksi berjalan</a>
				<a href="" class="list-group-item list-group-item-action waves-effect">Pembelian <i>Spare Parts</i></a>
				<a href="#" class="list-group-item list-group-item active waves-effect">Perbaikan / <i>Services</i></a>
			</div>
                
		</div> <!-- /.col-md -->
		
		<div class="col-md">
				
			<div class="row">
				<!-- MAIN CONTENT -->
				<?php if (validation_errors()) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (isset($error)) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= $error ?>
						</div>
					</div>
				<?php endif; ?>
				</div>
				
				<!-- INPUT SECTION -->
				<div class="row">
					<div class="col-md card">
						<div class="card-body">
							<?php echo form_open(base_url('index.php/home/search')); ?>
			        <input class="form-control" type="Cari" placeholder="kode barang" aria-label="search" name="search">
							<input class="btn btn-primary btn-md" type="button" value="Cari">
							<?php echo form_close(); ?>
						</div>
					</div>
					<!-- second card column -->
					<div class="col-md card">
						<div class="card-body">
							<?php echo form_open(base_url('index.php/home/search')); ?>
								<div class="form-group">
								  <select class="form-control" id="SERVIS	">
								  	<!-- foreach kategori -->
								  	<?php //foreach ($kategory as $dataCat): ?>
								  		
								  	<?php //endforeach ?>
								    <option>Pilih Kategori Servis</option>
								    <option>Kategori 1</option>
								    <option>Kategori 2</option>
								    <option>Kategori 3</option>
								    <option>Kategori 4</option>
								    <option>Kategori 5</option>
								  </select>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
					<!-- third card column -->
					<div class="col-md card">
						<div class="card-body">
							<h3>data pelanggan</h3><br>
							<p>Nama Pelanggan : <?php echo $this->session->userdata('plgNama'); ?></p>
							<p>Nomor Transaksi : <?php echo $this->session->userdata('plgNama'); ?></p>
							<p>Waktu : <?php echo $this->session->userdata('plgWaktu'); ?></p>
							<p>Jenis Kendaraan : <?php echo $this->session->userdata('plgKendaraan'); ?></p>
							<p>Nomor Kendaraan : <?php echo $this->session->userdata('plgNopol'); ?></p>
							<p>Nomor Telepon : <?php echo $this->session->userdata('plgTelepon'); ?></p>
						</div>

					</div>
				</div> <!-- /row -->

				<br><hr>

				<!-- /.INPUT SECTION -->

				<!-- Table for search -->
				
				<div class="row"> <!-- table row -->
					<div class="card card-body">
					<!--Table-->
					<table class="table table-hover">

				    <!--Table head-->
				    <thead class="mdb-color darken-3 text-white">
				        <tr>
				            <th>No.</th>
				            <th>Kode Barang</th>
				            <th>Harga</th>
				            <th>Qty</th>
				            <th></th>
				        </tr>
				    </thead>
				    <!--Table head-->

				    <!--Table body-->
				    <tbody>
				    	<?php //foreach ($data as $datum): ?>
				    		
				    	<?php //endforeach ?>
				        <tr>
				            <th scope="row">1</th>
										<td>isi Kode Barang 1</td>
										<td>isi harga</td>
										<td>isi QTY</td>
										<td><a class="btn btn-unique btn-sm" href="#">Tambah</a></td>
				        </tr>
				        <tr>
				            <th scope="row">1</th>
										<td>isi Kode Barang 1</td>
										<td>isi harga</td>
										<td>isi QTY</td>
										<td><a class="btn btn-unique btn-sm" href="#">Tambah</a></td>
				        </tr>
				        <tr>
				            <th scope="row">1</th>
										<td>isi Kode Barang 1</td>
										<td>isi harga</td>
										<td>isi QTY</td>
										<td><a class="btn btn-unique btn-sm" href="#">Tambah</a></td>
				        </tr>
				    </tbody>
				    <!--Table body-->

					</table>
					<!--Table-->

					</div>
				</div> <!-- /table row -->
				<!-- /table for parts -->
				<hr>
				<!-- Second table for purchased services and parts -->
				<div class="row"> <!-- table row -->
					<div class="card card-body">
					<!--Table-->
					<table class="table table-hover">

				    <!--Table head-->
				    <thead class="mdb-color darken-3 text-white">
				        <tr>
				            <th>No.</th>
				            <th>Nama Barang</th>
				            <th>Harga</th>
				            <th>Jumlah</th>
				        </tr>
				    </thead>
				    <!--Table head-->

				    <!--Table body-->
				    <tbody>
				        <tr>
				           <th scope="row">1</th>
										<td>isi kolom 1</td>
										<td>isi kolom 2</td>
										<td>isi kolom 3</td>
				        </tr>
				        <tr>
				            <th scope="row">1</th>
										<td>isi kolom 1</td>
										<td>isi kolom 2</td>
										<td>isi kolom 3</td>
				        </tr>
				        <tr>
				            <th scope="row">1</th>
										<td>isi kolom 1</td>
										<td>isi kolom 2</td>
										<td>isi kolom 3</td>
				        </tr>
				    </tbody>
				    <!--Table body-->

					</table>
					<!--Table-->

					</div>
				</div> <!-- /table row -->
				<!-- /MAIN CONTENT -->

		</div> <!-- /container2 -->
	</div> <!-- /row -->
</div> <!-- /container1 -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Daftar Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                	<caption>table title and/or explanatory text</caption>
                	<thead class="mdb-color darken-3 text-white">
                		<tr>
                			<th colspan="1">No</th>
                			<th colspan="2">Nama Barang</th>
                			<th colspan="2">Kode Barang</th>
                		</tr>
                	</thead>
                	<tbody>
										<tr>
											<td>1</td>
											<td colspan="2">lorem</td>
											<td colspan="2">ipsum</td>
										</tr>
										<tr>
											<td>2</td>
											<td colspan="2">lorem</td>
											<td colspan="2">ipsum</td>
										</tr>
                	</tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>