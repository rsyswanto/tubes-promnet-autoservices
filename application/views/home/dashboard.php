<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
			<div class="list-group">
		    <a href="" class="list-group-item active waves-effect">Transaksi berjalan</a>
				<a href="#" class="list-group-item list-group-item-action waves-effect">Pembelian <i>Spare Parts</i></a>
				<a href="<?php echo base_url('index.php/home/transaction') ?>" class="list-group-item list-group-item-action waves-effect">Perbaikan / <i>Services</i></a>
			</div>    
		</div> <!-- /.col-md -->

		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="col-md">
				
			<div class="row">
				<!-- MAIN CONTENT -->
				<?php if (validation_errors()) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (isset($error)) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= $error ?>
						</div>
					</div>
				<?php endif; ?>
				</div>
				
				<!-- HEAD -->
				<div class="row">
					<div class="col-md">
						<h1>Transaksi</h1>
					</div>
					<div class="col-md">
						<a href="" class="btn btn-primary btn-rounded mb-4" data-toggle="modal" data-target="#modalRegisterForm">Tambah Transaksi</a>
					</div>
				</div> <!-- /row -->
				<!-- /.HEAD -->
				<hr>
				
				<div class="row"> <!-- table row -->
					<div class="card card-body">
					<!--Table-->
					<table class="table table-hover">
				    <!--Table head-->
				    <thead class="mdb-color darken-3 text-white">
				        <tr>
				            <th>No</th>
				            <th>Nomor Transaksi</th>
				            <th>Nama Pelanggan</th>
				            <th>Waktu Masuk</th>
				            <th>Status</th>
				            <th>Pilihan</th>
				        </tr>
				    </thead>
				    <!--Table head-->

				    <!--Table body-->
				    <tbody>
				        <tr>
				            <th scope="row">1</th>
										<td>isi kolom 2</td>
										<td>isi kolom 3</td>
										<td>isi kolom 4</td>
										<td>isi kolom 5</td>
										<td><a class="btn btn-unique btn-sm" href="#">lihat</a> <a class="btn btn-danger btn-sm" href="#">hapus</a></td>
				        </tr>
				        <tr>
				            <th scope="row">1</th>
										<td>isi kolom 2</td>
										<td>isi kolom 3</td>
										<td>isi kolom 4</td>
										<td>isi kolom 5</td>
										<td><a class="btn btn-unique btn-sm" href="#">lihat</a> <a class="btn btn-danger btn-sm" href="#">hapus</a></td>
				        </tr>
				        <tr>
				            <th scope="row">1</th>
										<td>isi kolom 2</td>
										<td>isi kolom 3</td>
										<td>isi kolom 4</td>
										<td>isi kolom 5</td>
										<td><a class="btn btn-unique btn-sm" href="#">lihat</a> <a class="btn btn-danger btn-sm" href="#">hapus</a></td>
				        </tr>
				    </tbody>
				    <!--Table body-->

					</table>
					<!--Table-->

					</div>
				</div> <!-- /table row -->
				<hr>
				<!-- Second table for search purpose -->
				<div class="row"> <!-- table row -->
					<div class="card card-body">
					<!--Table-->
						<h2>second row</h2>
					</div>
				</div> <!-- /table row -->
				<!-- /MAIN CONTENT -->

		</div> <!-- /container2 -->
	</div> <!-- /row -->
</div> <!-- /container -->

<!-- MODAL -->
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Data Pelanggan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open(base_url('index.php/home/tambahTransaksi')); ?>
            <div class="modal-body mx-4">
                <div class="md-form mb-5">
                    <i class="fa fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-name" class="form-control validate" name="fnama">
                    <label data-error="wrong" data-success="right" for="orangeForm-name">nama Pelanggan</label>
                </div>
                <div class="md-form mb-5">
                    <i class="fa fa-car prefix grey-text"></i>
                    <input type="text" id="orangeForm-vehicle" class="form-control validate" name="fkendaraan">
                    <label data-error="wrong" data-success="right" for="orangeForm-vehicle">Jenis Kendaraan</label>
                </div>

                <div class="md-form mb-5">
                    <i class="fa fa-vcard prefix grey-text"></i>
                    <input type="text" id="orangeForm-nopol" class="form-control validate" name="fnopol">
                    <label data-error="wrong" data-success="right" for="orangeForm-nopol">Nomor Polisi</label>
                </div>

                <div class="md-form mb-5">
                    <i class="fa fa-phone prefix grey-text"></i>
                    <input type="text" id="orangeForm-phone" class="form-control validate" name="ftelepon">
                    <label data-error="wrong" data-success="right" for="orangeForm-phone">Nomor Telepon</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <input class="btn btn-unique" type="submit" value="Lanjut">
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- /.MODAL -->