<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
			
			<div class="list-group">
			    <a href="#" class="list-group-item active waves-effect">
			        Cras justo odio
			    </a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Dapibus ac facilisis in</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Morbi leo risus</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Porta ac consectetur ac</a>
			    <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
			</div>
                
		</div> <!-- /.col-md -->

		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
			<MAIN>
				
				<!-- main content -->

			</MAIN>
			</div> <!-- /.row -->
		</div> <!-- /.container2 -->
	</div> <!-- /row -->
</div> <!-- /container -->