<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		// code
	}

	/**
	 * create_user function.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function create_cashier($username,$password,$nama)
	{
		// data object
		$data = array(
			'username'   		=> $username,
			'password'   		=> $this->hash_password($password),
			'nama'					=> $nama,
			'is_cashier'		=> 1,
			'created_at' 		=> date('Y-m-j H:i:s'),
		);

		return $this->db->insert('users', $data);
	}


	public function create_admin($username,$password,$name)
	{
		$data = array(
			'username'   		=> $username,
			'password'   		=> $this->hash_password($password),
			'nama'					=> $name,
			'is_admin'			=> 1,
			'created_at' 		=> date('Y-m-j H:i:s'),
		);

		return $this->db->insert('users', $data);
	}

	public function create_mechanic()
	{
		# code...
	}

	/**
	 * get_user_detail function
	 * get all attribute
	 * 
	 * @access public
	 * @return object
	 */
	public function get_user_detail($user_id)
	{
		$this->db->from('users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
	}


	public function get_user_id_from_username($username)
	{
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('username', $username);

		return $this->db->get()->row('id');
	}

	/**
 	 * create_user function.
	 * verify hashed password
	 * 
	 * @access public
	 * @return boolean
	 */
	public function pass_check($username, $password)
	{
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
	}

	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param string $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}

	/**
	 * 
	 * @access private
	 * @return boolean
	 */
	private function verify_password_hash($password, $hash) {
		return password_verify($password, $hash);
	}

	public function del_cashier()
	{
		# code...
	}

	public function del_mechanic()
	{
		# code...
	}
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */